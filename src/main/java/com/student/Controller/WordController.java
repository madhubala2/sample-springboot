package com.student.Controller;

import com.student.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("words")
public class WordController {

    @Autowired
    WordService wordService;

    @PostMapping("/{word}")
    public String add(@PathVariable String word) {
        return wordService.addWord(word);
    }

    @GetMapping
    public List<String> getAllWords(){
        return wordService.getAllWords();
    }

    @GetMapping("/{position}")
    public String getSpecificWord(@PathVariable int position){
        return wordService.getWord(position);
    }

    @DeleteMapping("/{position}")
    public String deleteWord(@PathVariable int position){
        return wordService.deleteWord(position);
    }
}
