package com.student.service;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WordService {

    private static List<String> list= new ArrayList<>();

    public String addWord(String word) {
        list.add(word);
        return "Added  "+word;
    }

    public List<String> getAllWords() {
        return list;
    }

    public String getWord(int position) {
        return list.get(position);
    }

    public String deleteWord(int position){
        String word = list.remove(position);
        return "Deleted "+word;
    }

}
